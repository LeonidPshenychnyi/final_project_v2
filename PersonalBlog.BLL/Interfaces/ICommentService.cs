﻿using System.Collections.Generic;
using PersonalBlog.BLL.DTO;

namespace PersonalBlog.BLL.Interfaces
{
    public interface ICommentService
    {
        void MakeComment(int postId, string commentText, string userId, string userLogin);
        CommentDTO getComment(int? id);
        IEnumerable<CommentDTO> GetComments();
        void DeleteComment(int commentId);
        void Dispose();
    }
}