﻿using System;
using System.Collections;
using System.Collections.Generic;
using PersonalBlog.BLL.DTO;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.BLL.Interfaces
{
    public interface IPostService
    {
        void MakePost(Guid UserId, string postText, string tagsText, string UserLogin);
        PostDTO GetPost(int? id);
        IEnumerable<PostDTO> GetPosts();
        void ChangePostText(string postId, string postText);
        void Dispose();
    }
}