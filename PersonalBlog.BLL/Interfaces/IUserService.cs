﻿using System;

namespace PersonalBlog.BLL.Interfaces
{
    public interface IUserService
    {
        bool DateValid(DateTime? date);
    }
}