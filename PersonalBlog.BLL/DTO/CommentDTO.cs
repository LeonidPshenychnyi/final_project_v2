﻿using System;

namespace PersonalBlog.BLL.DTO
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public Guid Userid { get; set; }
        public string UserLogin { get; set; }
        public int PostId { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
    }
}