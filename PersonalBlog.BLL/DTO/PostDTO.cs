﻿using System;
using System.Collections.Generic;

namespace PersonalBlog.BLL.DTO
{
    public class PostDTO
    {
        public int Id { get; set; }
        public DateTime? PostTime { get; set; }
        
        public string PostText { get; set; }
        public string TagsText { get; set; }
        public int CommentsCount { get; set; }
        public List<int> Comments { get; set; }
        
        public Guid UserId { get; set; }
        public string UserLogin { get; set; }
    }
}