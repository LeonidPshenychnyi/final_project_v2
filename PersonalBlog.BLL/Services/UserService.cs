﻿using System;
using PersonalBlog.BLL.Interfaces;

namespace PersonalBlog.BLL.Services
{
    public class UserService : IUserService
    {
        public bool DateValid(DateTime? date)
        {
            return date < DateTime.Now;
        }
    }
}