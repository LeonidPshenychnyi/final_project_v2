﻿using System;
using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using PersonalBlog.BLL.DTO;
using PersonalBlog.BLL.Interfaces;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.BLL.Services
{
    public class PostService : IPostService
    {
        private IPostService _postServiceImplementation;
        private IUnitOfWork Database { get; set; }

        public PostService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void MakePost(Guid userId, string postText, string tagsText, string userlogin)
        {
            postText = postText.Replace("<", "&lt;");
            postText = postText.Replace(">", "&gt;");
            postText = postText.Replace("\"", "&quot;");
            Post post = new Post
            {
                TagsText = tagsText,
                UserId = userId,
                UserLogin = userlogin,
                CommentsCount = 0,
                LikesCount = 0,
                PostText = postText,
                PostTime = DateTime.UtcNow,
            };
            Database.Posts.Create(post);
            Database.Save();
        }

        public IEnumerable<PostDTO> GetPosts()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(Database.Posts.GetAll());
        }

        public PostDTO GetPost(int? id)
        {
            var post = Database.Posts.Get(id.Value);
            
            return new PostDTO{Comments = post.Comments, CommentsCount = post.CommentsCount, PostText = post.PostText, PostTime = post.PostTime, UserId = post.UserId, Id = post.Id, UserLogin = post.UserLogin/*, TagsText = post.TagsText*/};
        }

        public void ChangePostText(string postId, string postText)
        { 
            var post = Database.Posts.Get(Convert.ToInt32(postId));
            post.PostText = postText;
            
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}