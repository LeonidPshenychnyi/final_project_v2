﻿using System;
using System.Collections.Generic;
using AutoMapper;
using PersonalBlog.BLL.DTO;
using PersonalBlog.BLL.Interfaces;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.BLL.Services
{
    public class CommentService : ICommentService
    {
        private IUnitOfWork Database { get; set; }

        public CommentService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void MakeComment(int postId, string commentText, string userId, string userLogin)
        {
            commentText = commentText.Replace("<", "&lt;");
            commentText = commentText.Replace(">", "&gt;");
            commentText = commentText.Replace("\"", "&quot;");
            Comment comment = new Comment
            {
                PostId = postId,
                Text = commentText,
                PostDate = DateTime.UtcNow,
                Userid = Guid.Parse(userId),
                UserLogin = userLogin
            };
            Database.Comments.Create(comment);
            Database.Save();
        }

        public IEnumerable<CommentDTO> GetComments()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Comment>, List<CommentDTO>>(Database.Comments.GetAll());
        }

        public CommentDTO getComment(int? id)
        {
            var post = Database.Comments.Get(id.Value);
            
            return new CommentDTO{PostId = post.PostId, PostDate = post.PostDate, Text = post.Text};
        }
        
        public void DeleteComment(int commentId)
        {
            Database.Comments.Delete(commentId);
            
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}