﻿using Ninject.Modules;
using PersonalBlog.DAL.Interfaces;
using PersonalBlog.DAL.Repositories;

namespace PersonalBlog.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;

        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EfUnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}