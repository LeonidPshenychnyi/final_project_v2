﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PersonalBlog.BLL.Infrastructure
{
    public class BirthDateValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var val = (DateTime)value;

            if (val > DateTime.Now)
                return false;

            return true;
        }
    }
}