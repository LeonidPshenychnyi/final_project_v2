﻿using System.Web.Mvc;
using PersonalBlog.DAL.Identity;

namespace PersonalBlog.Helpers
{
    public static class UserDataHelper
    {
        /// <summary>
        /// Shows user's data
        /// </summary>
        /// <param name="html">Extend Html</param>
        /// <param name="userElement">Element of user</param>
        /// <returns>User's data html</returns>
        public static MvcHtmlString UserData(this HtmlHelper html, ApplicationUser userElement)
        {
            var divider = new TagBuilder("div");
            divider.AddCssClass("divider");
            
            var Posts = new TagBuilder("div");
            
            var divUser = new TagBuilder("div");
            divUser.AddCssClass("col");
            divUser.AddCssClass("m12");
            divUser.AddCssClass("offset-m0");
            divUser.AddCssClass("z-depth-2");
            divUser.AddCssClass("deep-purple");
            divUser.AddCssClass("lighten-4");
            
            var pCity = new TagBuilder("p");
            pCity.InnerHtml += "<strong>City: </strong>";

            if (!string.IsNullOrEmpty(userElement.City))
            {
                pCity.InnerHtml += userElement.City + " ";
            }

            var pLanguages = new TagBuilder("p");
            pLanguages.InnerHtml += "<strong>Languages: </strong>";

            if (!string.IsNullOrEmpty(userElement.Languages))
            {
                pLanguages.InnerHtml += userElement.Languages + " ";
            }

            var pAbout = new TagBuilder("p");
            pAbout.InnerHtml += "<strong>About: </strong>";

            if (!string.IsNullOrEmpty(userElement.About))
            {
                pAbout.InnerHtml += userElement.About + " ";
            }

            divUser.InnerHtml += pCity.ToString();
            divUser.InnerHtml += divider.ToString();

            divUser.InnerHtml += pLanguages.ToString();
            divUser.InnerHtml += divider.ToString();

            divUser.InnerHtml += pAbout.ToString();

            Posts.InnerHtml += divUser.ToString();
            Posts.InnerHtml += divider;
            
            return new MvcHtmlString(Posts.ToString());
        }
    }
}