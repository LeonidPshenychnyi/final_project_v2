﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PersonalBlog.BLL.DTO;
using PersonalBlog.DAL.Entities;
using PersonalBlog.Models;

namespace PersonalBlog.Helpers
{
    public static class PostCommentHelper
    {
        /// <summary>
        /// Creates comments
        /// </summary>
        /// <param name="html">Extend Html</param>
        /// <param name="comments">List of comments</param>
        /// <param name="pageUrl">Url that takes string to DeleteComment</param>
        /// <param name="curUser">ViewModel of current user</param>
        /// <returns>Comment's page</returns>
        public static MvcHtmlString PostCommentGenerator(this HtmlHelper html, IEnumerable<Comment> comments, Func<string, string> pageUrl, UserViewModel curUser)
        {
            var Posts = new TagBuilder("div");
            
            foreach (var comment in comments.OrderByDescending(o => o.PostDate))
            {
                var post = new TagBuilder("div");
                post.AddCssClass("col");
                post.AddCssClass("m12");
                post.AddCssClass("offset-m0");
                post.AddCssClass("z-depth-2");
                post.AddCssClass("deep-purple");
                post.AddCssClass("lighten-4");

                var clearSpace = new TagBuilder("div");
                clearSpace.AddCssClass("clear-15");
                Posts.InnerHtml += clearSpace.ToString();

                var topMargin = new TagBuilder("div");
                topMargin.AddCssClass("clear-10");
                post.InnerHtml += topMargin.ToString();

                var ulCollection = new TagBuilder("ul");
                ulCollection.AddCssClass("collection");
                ulCollection.AddCssClass("avatarUlHelper");

                var liElement = new TagBuilder("li");
                liElement.AddCssClass("collection-item");
                liElement.AddCssClass("avatar");
                liElement.AddCssClass("avatarLiHelper");

                var imgAvatar = new TagBuilder("img");
                imgAvatar.Attributes.Add("src", "/Content/images/testAvatar.png");
                imgAvatar.AddCssClass("circle");
                imgAvatar.AddCssClass("avatarCircleHelper");

                var titleSpan = new TagBuilder("span");
                titleSpan.AddCssClass("title");
                titleSpan.AddCssClass("login-text");
                titleSpan.InnerHtml += comment.UserLogin;

                var pTime = new TagBuilder("p");
                pTime.InnerHtml += comment.PostDate.ToString();

                //Button
                var aEdit = new TagBuilder("a");
                aEdit.InnerHtml += "Delete";
                aEdit.MergeAttribute("href", pageUrl(Convert.ToString(comment.Id)));
                aEdit.AddCssClass("red");
                aEdit.AddCssClass("btn");
                aEdit.AddCssClass(".light-green.accent-3");
                aEdit.AddCssClass("waves-light");
                aEdit.AddCssClass("waves-effect");
                aEdit.AddCssClass("secondary-content");

                var imgDots = new TagBuilder("img");
                imgDots.Attributes.Add("src", "/Content/images/dots.png");
                imgDots.Attributes.Add("style", "width: 30px; height: 30px;");
                
                string pTest = comment.Text;
                pTest = pTest.Replace("<", "&lt;");
                pTest = pTest.Replace(">", "&gt;");
                pTest = pTest.Replace("\"", "&quot;");
                var pText = new TagBuilder("p");
                pText.InnerHtml += pTest.ToString();

                liElement.InnerHtml += imgAvatar.ToString();
                liElement.InnerHtml += titleSpan.ToString();
                liElement.InnerHtml += pTime.ToString();
                liElement.InnerHtml += pText.ToString();
                
                if (curUser.Id == Convert.ToString(comment.Userid) || curUser.Role == "Admin")
                liElement.InnerHtml += aEdit.ToString();

                ulCollection.InnerHtml += liElement.ToString();

                post.InnerHtml += ulCollection.ToString();
                
                Posts.InnerHtml += post.ToString();
            }

            return new MvcHtmlString(Posts.ToString());
        }
    }
}