﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;

namespace PersonalBlog.Helpers
{
    public static class FindUsersHelper
    {
        /// <summary>
        /// Shows users
        /// </summary>
        /// <param name="html">Extend html</param>
        /// <param name="users">List of users</param>
        /// <param name="loginPart">Part of login</param>
        /// <param name="pageUrl">Url that takes string to PersonalBlog</param>
        /// <returns>Users page</returns>
        public static MvcHtmlString FindUsers(this HtmlHelper html, IEnumerable<ApplicationUser> users, string loginPart, Func<string, string> pageUrl)
        {
            if (loginPart == null)
            {
                return null;
            }
            var Posts = new TagBuilder("div");
            foreach (var text in users.Where(w => w.Login.ToLower().Contains(loginPart.ToLower())).OrderBy(o => o.Login.ToLower().Length))
            {
                var post = new TagBuilder("div");
                post.AddCssClass("col");
                post.AddCssClass("m12");
                post.AddCssClass("offset-m0");
                post.AddCssClass("z-depth-2");
                post.AddCssClass("deep-purple");
                post.AddCssClass("lighten-4");

                var clearSpace = new TagBuilder("div");
                clearSpace.AddCssClass("clear-15");
                Posts.InnerHtml += clearSpace.ToString();

                var topMargin = new TagBuilder("div");
                topMargin.AddCssClass("clear-10");
                post.InnerHtml += topMargin.ToString();

                var ulCollection = new TagBuilder("ul");
                ulCollection.AddCssClass("collection");
                ulCollection.AddCssClass("avatarUlHelper");

                var liElement = new TagBuilder("li");
                liElement.AddCssClass("collection-item");
                liElement.AddCssClass("avatar");
                liElement.AddCssClass("avatarLiHelper");

                var imgAvatar = new TagBuilder("img");
                imgAvatar.Attributes.Add("src", "/Content/images/testAvatar.png");
                imgAvatar.AddCssClass("circle");
                imgAvatar.AddCssClass("avatarCircleHelper");

                var titleSpan = new TagBuilder("span");
                titleSpan.AddCssClass("title");
                titleSpan.AddCssClass("login-text");
                titleSpan.InnerHtml += text.FirstName + " " + text.LastName;

                var pLogin = new TagBuilder("p");
                var aLogin = new TagBuilder("a");
                aLogin.InnerHtml += "@" + text.Login;
                aLogin.MergeAttribute("href", pageUrl(text.Login));
                pLogin.InnerHtml += aLogin.ToString();

                liElement.InnerHtml += imgAvatar.ToString();
                liElement.InnerHtml += titleSpan.ToString();
                liElement.InnerHtml += pLogin.ToString();

                ulCollection.InnerHtml += liElement.ToString();

                post.InnerHtml += ulCollection.ToString();

                var divider = new TagBuilder("div");
                divider.AddCssClass("divider");
                post.InnerHtml += divider.ToString();

                Posts.InnerHtml += post.ToString();
            }
            return new MvcHtmlString(Posts.ToString());
        }
    }
}