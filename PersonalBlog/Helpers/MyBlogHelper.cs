﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PersonalBlog.BLL.DTO;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;

namespace PersonalBlog.Helpers
{
    public static class MyBlogHelper
    {
        /// <summary>
        /// Generates user's posts
        /// </summary>
        /// <param name="html">Extend Html</param>
        /// <param name="posts">List of posts</param>
        /// <param name="UserId">Id of user</param>
        /// <param name="pageUrl">Url that takes string to EditPost</param>
        /// <param name="postUrl">Url that takes string to PostComments</param>
        /// <param name="comments">List of comments</param>
        /// <param name="isMe">If it's user</param>
        /// <returns>User's page</returns>
        public static MvcHtmlString MyPostsGenerator(this HtmlHelper html, List<Post> posts, Guid UserId,  Func<string, string> pageUrl, Func<string, string> postUrl, IEnumerable<CommentDTO> comments, bool isMe)
        {
            var divider = new TagBuilder("div");
            divider.AddCssClass("divider");
            
            posts.Reverse();
            var Posts = new TagBuilder("div");
            
            foreach (var text in posts.Where(w => w.UserId == UserId))
            {
                string pTest = text.PostText;
                pTest = pTest.Replace("<", "&lt;");
                pTest = pTest.Replace(">", "&gt;");
                pTest = pTest.Replace("\"", "&quot;");
                
                var post = new TagBuilder("div");
                post.AddCssClass("col");
                post.AddCssClass("m12");
                post.AddCssClass("offset-m0");
                post.AddCssClass("z-depth-2");
                post.AddCssClass("deep-purple");
                post.AddCssClass("lighten-4");

                var clearSpace = new TagBuilder("div");
                clearSpace.AddCssClass("clear-15");
                Posts.InnerHtml += clearSpace.ToString();

                var topMargin = new TagBuilder("div");
                topMargin.AddCssClass("clear-10");
                post.InnerHtml += topMargin.ToString();

                var ulCollection = new TagBuilder("ul");
                ulCollection.AddCssClass("collection");
                ulCollection.AddCssClass("avatarUlHelper");

                var liElement = new TagBuilder("li");
                liElement.AddCssClass("collection-item");
                liElement.AddCssClass("avatar");
                liElement.AddCssClass("avatarLiHelper");

                var imgAvatar = new TagBuilder("img");
                imgAvatar.Attributes.Add("src", "/Content/images/testAvatar.png");
                imgAvatar.AddCssClass("circle");
                imgAvatar.AddCssClass("avatarCircleHelper");

                var titleSpan = new TagBuilder("span");
                titleSpan.AddCssClass("title");
                titleSpan.AddCssClass("login-text");
                titleSpan.InnerHtml += text.UserLogin.ToString();

                var pTime = new TagBuilder("p");
                pTime.InnerHtml += text.PostTime.ToString();

                var aEdit = new TagBuilder("a");
                aEdit.InnerHtml += "Edit";
                aEdit.MergeAttribute("href", pageUrl(Convert.ToString(text.Id)));
                aEdit.AddCssClass("green");
                aEdit.AddCssClass("btn");
                aEdit.AddCssClass(".light-green.accent-3");
                aEdit.AddCssClass("waves-light");
                aEdit.AddCssClass("waves-effect");
                aEdit.AddCssClass("secondary-content");

                liElement.InnerHtml += imgAvatar.ToString();
                liElement.InnerHtml += titleSpan.ToString();
                liElement.InnerHtml += pTime.ToString();
                if (isMe)
                {
                    liElement.InnerHtml += aEdit.ToString();
                }

                ulCollection.InnerHtml += liElement.ToString();

                post.InnerHtml += ulCollection.ToString();
                
                post.InnerHtml += divider.ToString();
                
                var postTextDemo = new TagBuilder("span");
                if (!string.IsNullOrEmpty(text.PostText))
                {
                    var demoString = pTest.Substring(0, Math.Min(pTest.Length, 200));
                    var lastString = pTest.Substring(Math.Min(pTest.Length, 200));

                    postTextDemo.InnerHtml += demoString;
                    postTextDemo.AddCssClass("post-text");
                    post.InnerHtml += postTextDemo.ToString();

                    if (lastString.Length > 0)
                    {
                        var readMoreA = new TagBuilder("a");
                        readMoreA.InnerHtml += " Read more...";
                        readMoreA.AddCssClass("dots");
                        readMoreA.AddCssClass("read");
                        readMoreA.AddCssClass("post-text");
                        post.InnerHtml += readMoreA.ToString();

                        var postTextLast = new TagBuilder("span");
                        postTextLast.InnerHtml += lastString;
                        postTextLast.AddCssClass("more");
                        postTextLast.AddCssClass("post-text");
                        post.InnerHtml += postTextLast.ToString();
                    }
                    var tags = text.TagsText;
                    if (!string.IsNullOrEmpty(tags))
                    {
                        var tagsArr = tags.Split('|');

                        var divTags = new TagBuilder("div");
                        foreach (var tag in tagsArr)
                        {
                            var spanTag = new TagBuilder("span");
                            spanTag.AddCssClass("chip");
                            spanTag.InnerHtml += "#" + tag;
                            if (!string.IsNullOrEmpty(tag))
                            {
                                divTags.InnerHtml += spanTag.ToString();
                            }
                        }

                        post.InnerHtml += divTags.ToString();
                    }

                    post.InnerHtml += divider.ToString();

                    var divDownBar = new TagBuilder("div");

                    var spanDownBarImg = new TagBuilder("span");

                    var imgCommentDownbar = new TagBuilder("img");
                    var aCommentDownbar = new TagBuilder("a");
                    aCommentDownbar.MergeAttribute("href", postUrl(Convert.ToString(text.Id)));
                    imgCommentDownbar.Attributes.Add("src", "/Content/images/comment.png");
                    imgCommentDownbar.AddCssClass("downbarCircleHelper");
                    aCommentDownbar.InnerHtml += imgCommentDownbar.ToString();
                    spanDownBarImg.InnerHtml += aCommentDownbar.ToString();
                    spanDownBarImg.InnerHtml += comments.Count(w => w.PostId == text.Id).ToString();

                    divDownBar.InnerHtml += spanDownBarImg.ToString();

                    post.InnerHtml += divDownBar.ToString();

                    Posts.InnerHtml += post.ToString();
                }
            }
            return new MvcHtmlString(Posts.ToString());
        }
    }
}