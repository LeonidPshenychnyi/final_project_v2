﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PersonalBlog.BLL.DTO;
using PersonalBlog.DAL.Entities;

namespace PersonalBlog.Helpers
{
    public static class BlogHelper
    {
        /// <summary>
        /// Creates posts
        /// </summary>
        /// <param name="html">Extend html</param>
        /// <param name="posts">List of posts</param>
        /// <param name="pageUrl">Url that takes int to PostComments</param>
        /// <param name="pageUrl2">Url that takes string to EditPost</param>
        /// <param name="userUrl3">Url that takes string to PersonalBlog</param>
        /// <param name="comments">List of comments</param>
        /// <param name="findText">That that finds in tags or text in Posts</param>
        /// <param name="isAdmin">If user is admin is true</param>
        /// <returns>Posts page</returns>
        public static MvcHtmlString PostGenerator(this HtmlHelper html, List<Post> posts, Func<int, string> pageUrl, Func<string, string> pageUrl2, Func<string, string> userUrl3,
            IEnumerable<CommentDTO> comments, string findText, bool isAdmin)
        {
            posts.Reverse();
            var Posts = new TagBuilder("div");
            foreach (var element in posts.Where(w => w.PostText.ToLower().Contains(findText.ToLower()) || w.TagsText.ToLower().Contains(findText.ToLower())))
            {
                var post = new TagBuilder("div");
                post.AddCssClass("col");
                post.AddCssClass("m12");
                post.AddCssClass("offset-m0");
                post.AddCssClass("z-depth-2");
                post.AddCssClass("deep-purple");
                post.AddCssClass("lighten-4");

                var clearSpace = new TagBuilder("div");
                clearSpace.AddCssClass("clear-15");
                Posts.InnerHtml += clearSpace.ToString();
                
                var topMargin = new TagBuilder("div");
                topMargin.AddCssClass("clear-10");
                post.InnerHtml += topMargin.ToString();

                var ulCollection = new TagBuilder("ul");
                ulCollection.AddCssClass("collection");
                ulCollection.AddCssClass("avatarUlHelper");

                var liElement = new TagBuilder("li");
                liElement.AddCssClass("collection-item");
                liElement.AddCssClass("avatar");
                liElement.AddCssClass("avatarLiHelper");

                var imgAvatar = new TagBuilder("img");
                imgAvatar.Attributes.Add("src", "/Content/images/testAvatar.png");
                imgAvatar.AddCssClass("circle");
                imgAvatar.AddCssClass("avatarCircleHelper");

                var aLogin = new TagBuilder("a");
                aLogin.AddCssClass("title");
                aLogin.MergeAttribute("href", userUrl3(element.UserLogin));
                aLogin.AddCssClass("login-text");
                aLogin.AddCssClass("purple-text");
                aLogin.InnerHtml += "@" + element.UserLogin;

                var pTime = new TagBuilder("p");
                pTime.InnerHtml += element.PostTime.ToString();

                //Button
                var aEdit = new TagBuilder("a");
                aEdit.InnerHtml += "Edit";
                aEdit.MergeAttribute("href", pageUrl2(Convert.ToString(element.Id)));
                aEdit.AddCssClass("green");
                aEdit.AddCssClass("btn");
                aEdit.AddCssClass(".light-green.accent-3");
                aEdit.AddCssClass("waves-light");
                aEdit.AddCssClass("waves-effect");
                aEdit.AddCssClass("secondary-content");

                liElement.InnerHtml += imgAvatar.ToString();
                liElement.InnerHtml += aLogin.ToString();
                liElement.InnerHtml += pTime.ToString();

                if (isAdmin)
                {
                    liElement.InnerHtml += aEdit.ToString();
                }

                ulCollection.InnerHtml += liElement.ToString();

                post.InnerHtml += ulCollection.ToString();

                var divider = new TagBuilder("div");
                divider.AddCssClass("divider");
                post.InnerHtml += divider.ToString();

                var postTextDemo = new TagBuilder("span");
                var demoString = element.PostText.Substring(0, Math.Min(element.PostText.Length, 200));
                var lastString = element.PostText.Substring(Math.Min(element.PostText.Length, 200));

                postTextDemo.InnerHtml += demoString;
                postTextDemo.AddCssClass("post-text");
                post.InnerHtml += postTextDemo.ToString();

                if (lastString.Length > 0)
                {
                    var readMoreA = new TagBuilder("a");
                    readMoreA.InnerHtml += " Read more...";
                    readMoreA.AddCssClass("dots");
                    readMoreA.AddCssClass("read");
                    readMoreA.AddCssClass("post-text");
                    post.InnerHtml += readMoreA.ToString();

                    var postTextLast = new TagBuilder("span");
                    postTextLast.InnerHtml += lastString;
                    postTextLast.AddCssClass("more");
                    postTextLast.AddCssClass("post-text");
                    post.InnerHtml += postTextLast.ToString();
                }
                var tags = element.TagsText;
                if (!string.IsNullOrEmpty(tags))
                {
                    var tagsArr = tags.Split('|');

                    var divTags = new TagBuilder("div");
                    foreach (var tag in tagsArr)
                    {
                        var spanTag = new TagBuilder("span");
                        spanTag.AddCssClass("chip");
                        spanTag.InnerHtml += "#" + tag;
                        if (!string.IsNullOrEmpty(tag))
                        {
                            divTags.InnerHtml += spanTag.ToString();
                        }
                    }

                    post.InnerHtml += divTags.ToString();
                }

                post.InnerHtml += divider.ToString();

                var divDownBar = new TagBuilder("div");
                var spanDownBarImg = new TagBuilder("span");
                
                var imgCommentDownbar = new TagBuilder("img");
                var aCommentDownbar = new TagBuilder("a");
                imgCommentDownbar.Attributes.Add("src", "/Content/images/comment.png");
                imgCommentDownbar.AddCssClass("downbarCircleHelper");
                
                aCommentDownbar.MergeAttribute("href", pageUrl(element.Id));
                aCommentDownbar.InnerHtml += imgCommentDownbar.ToString();
                spanDownBarImg.InnerHtml += aCommentDownbar.ToString();
                spanDownBarImg.InnerHtml += comments.Count(w => w.PostId == element.Id).ToString();

                divDownBar.InnerHtml += spanDownBarImg.ToString();

                post.InnerHtml += divDownBar.ToString();

                Posts.InnerHtml += post.ToString();
            }
            return new MvcHtmlString(Posts.ToString());
        }
    }
}