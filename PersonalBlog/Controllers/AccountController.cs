﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PersonalBlog.DAL.Identity;
using PersonalBlog.Models;

namespace PersonalBlog.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// User manager
        /// </summary>
        private ApplicationUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        /// <summary>
        /// Authentication manager
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        /// <summary>
        /// Reload all Session data about user
        /// </summary>
        public void Datareload()
        {
            Session.Clear();
            var userId = User.Identity.GetUserId();
            var user = UserManager.Users.FirstOrDefault(w => w.Id == userId);
            Session["UserLogin"] = user.Login;
            Session["UserName"] = user.FirstName;
        }

        /// <summary>
        /// Go to Register View
        /// </summary>
        /// <returns>Registration view</returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Try to make registration
        /// </summary>
        /// <param name="model">Model from view</param>
        /// <returns>Exception or registration</returns>
        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            ApplicationUser user;
            if (ModelState.IsValid && model.Birthdate < DateTime.UtcNow)
            {
                if (model.Login == "Admin")
                {
                    user = new ApplicationUser
                    {
                        UserName = model.Email, Email = model.Email, Birthdate = model.Birthdate,
                        PhoneNumber = model.Phone, FirstName = model.FirstName, LastName = model.LastName,
                        Login = model.Login, RegistrationDate = DateTime.UtcNow, Role = "Admin"
                    };
                }
                else
                {
                    user = new ApplicationUser
                    {
                        UserName = model.Email, Email = model.Email, Birthdate = model.Birthdate,
                        PhoneNumber = model.Phone, FirstName = model.FirstName, LastName = model.LastName,
                        Login = model.Login, RegistrationDate = DateTime.UtcNow, Role = "User"
                    };
                }

                if (UserManager.Users.FirstOrDefault(w => w.Login == user.Login) != null)
                {
                    return RedirectToAction("Register");
                }

                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    Session["UserLogin"] = user.Login;
                    Session["UserName"] = user.FirstName;
                    return RedirectToAction("Login");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }

            return View(model);
        }

        /// <summary>
        /// Go to Login view
        /// </summary>
        /// <returns>Login view</returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Try to make login
        /// </summary>
        /// <param name="model">Model from view</param>
        /// <returns>Make login or exception</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Wrong login or password");
                }
                else
                {
                    ClaimsIdentity claim =
                        await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    Session["UserName"] = user.FirstName;
                    Session["UserLogin"] = user.Login;
                    return RedirectToAction("Index", "Blog");
                }
            }

            return View(model);
        }

        /// <summary>
        /// Log out from Session
        /// </summary>
        /// <returns>Make's logOut</returns>
        [HttpPost]
        public ActionResult LogOut()
        {
            Session.Clear();
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }
    }
}