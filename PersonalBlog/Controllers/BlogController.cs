﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PersonalBlog.BLL.DTO;
using PersonalBlog.BLL.Interfaces;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;
using PersonalBlog.Models;

namespace PersonalBlog.Controllers
{
    public class BlogController : Controller
    {
        /// <summary>
        /// Post service
        /// </summary>
        private IPostService _postService;
        
        /// <summary>
        /// Comment service
        /// </summary>
        private ICommentService _commentService;

        /// <summary>
        /// User manager
        /// </summary>
        private ApplicationUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        /// <summary>
        /// Reload all Session Data
        /// </summary>
        public void Datareload()
        {
            Session.Clear();
            TempData.Clear();
            var userId = User.Identity.GetUserId();
            var user = UserManager.Users.FirstOrDefault(w => w.Id == userId);
            Session["UserLogin"] = user.Login;
            Session["UserName"] = user.FirstName;
        }

        /// <summary>
        /// Constructor that make DI
        /// </summary>
        /// <param name="service">Post service</param>
        /// <param name="commentService">Comment service</param>
        public BlogController(IPostService service, ICommentService commentService)
        {
            _postService = service;
            _commentService = commentService;
        }

        /// <summary>
        /// Main page of blog
        /// </summary>
        /// <param name="findText">text to search in all posts</param>
        /// <returns>View of main blog</returns>
        [Authorize]
        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Index(FindtextViewModel findText)
        {
            Datareload();

            if (string.IsNullOrEmpty(findText.Text))
            {
                findText.Text = "";
            }

            var curUser = User.Identity.GetUserId();
            findText.isAdmin = UserManager.Users.SingleOrDefault(w => w.Id == curUser)?.Role == "Admin";

            IEnumerable<PostDTO> phoneDtos = _postService.GetPosts();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PostDTO, Post>()).CreateMapper();
            var phones = mapper.Map<IEnumerable<PostDTO>, List<Post>>(phoneDtos);
            TempData["AllPosts"] = phones;
            TempData["Comments"] = _commentService.GetComments();
            return View(findText);
        }

        /// <summary>
        /// Page of personal blog of user
        /// </summary>
        /// <param name="login">Login of user that page will shows</param>
        /// <returns>User's personal blog</returns>
        /// <exception cref="Exception">Don't found user</exception>
        [Authorize]
        public ActionResult PersonalBlog(string login)
        {
            try
            {
                if (string.IsNullOrEmpty((string) Session["UserLogin"]))
                {
                    Datareload();
                }

                IEnumerable<PostDTO> postsDtos = _postService.GetPosts();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PostDTO, Post>()).CreateMapper();
                var posts = mapper.Map<IEnumerable<PostDTO>, List<Post>>(postsDtos);
                TempData.Clear();
                TempData["Posts"] = posts;

                var postModel = new PostViewModel();
                if (string.IsNullOrEmpty(login))
                {
                    TempData["UserId"] = Guid.Parse(User.Identity.GetUserId());
                    postModel.IsMe = true;
                }
                else
                {
                    var curId = User.Identity.GetUserId();
                    var userId = UserManager.Users.FirstOrDefault(w => w.Login == login)?.Id;
                    var selectedId = UserManager.Users.SingleOrDefault(w => w.Id == curId)?.Id;
                    if (!string.IsNullOrEmpty(userId))
                    {
                        TempData["UserId"] = Guid.Parse(userId);
                        if (userId == selectedId)
                        {
                            postModel.IsMe = true;
                        }
                    }
                    else
                    {
                        throw new Exception("404");
                        // return RedirectToAction("FindUsers");
                    }
                }

                var curUserId = User.Identity.GetUserId();
                TempData["User"] = UserManager.Users.FirstOrDefault(w => w.Id == curUserId);
                TempData["Comments"] = _commentService.GetComments();
                return View(postModel);
            }
            catch
            {
                return View("Error");
            }
        }

        /// <summary>
        /// Page with user data and changing it
        /// </summary>
        /// <returns>Settings view</returns>
        [Authorize]
        public ActionResult UserSettings()
        {
            var userId = User.Identity.GetUserId();
            var user = UserManager.Users.FirstOrDefault(w => w.Id == userId);

            TempData["FirstName"] = user.FirstName;
            TempData["LastName"] = user.LastName;
            TempData["Birthdate"] = user.Birthdate;
            TempData["City"] = user.City;
            TempData["Languages"] = user.Languages;
            TempData["About"] = user.About;
            TempData["Email"] = user.Email;
            TempData["Phone"] = user.PhoneNumber;
            return View();
        }

        /// <summary>
        /// Try to change user Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Changing user data or exception</returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> ChangeUserData(UserViewModel model)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            string exception = "";
            if (string.IsNullOrEmpty(model.FirstName))
            {
                exception += "<p>Wrong user name</p>";
            }
            if (string.IsNullOrEmpty(model.LastName))
            {
                exception += "<p>Wrong user last name</p>";
            }
            if (model.BirthDate == default(DateTime) || model.BirthDate > DateTime.UtcNow)
            {
                exception += "<p>Wrong birthdate</p>";
            }
            // if (string.IsNullOrEmpty(UserManager.PasswordHasher.HashPassword(model.Password)))
            // {
            //     exception += "<p>Wrong password</p>";
            // }
            if (string.IsNullOrEmpty(model.Email) || !model.Email.Contains("@"))
            {
                exception += "<p>Wrong email address<p>";
            }
            if (string.IsNullOrEmpty(model.Phone))
            {
                exception += "<p>Wrong phone number</p>";
            }

            if (exception == "")
            {
                user.FirstName = model.FirstName;
                user.LastName = model.LastName; 
                user.City = model.City;
                user.Languages = model.Languages;
                user.About = model.About;
                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                user.Email = model.Email;
                user.Birthdate = model.BirthDate;
                user.PhoneNumber = model.Phone;
                user.UserName = model.Email;
                
                await UserManager.UpdateAsync(user);
            }

            return RedirectToAction("UserSettings");
        }

        /// <summary>
        /// Creating post
        /// </summary>
        /// <param name="model">Model from view</param>
        /// <returns>Creating post or exception</returns>
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreatePost(PostViewModel model)
        {
            if (string.IsNullOrEmpty((string) Session["UserLogin"]))
            {
                Datareload();
            }

            if (!string.IsNullOrEmpty(model.PostText))
            {
                if (string.IsNullOrEmpty(model.TagsText))
                {
                    _postService.MakePost(Guid.Parse(User.Identity.GetUserId()), model.PostText, "",
                        Convert.ToString(Session["UserLogin"]));
                }
                else
                {
                    _postService.MakePost(Guid.Parse(User.Identity.GetUserId()), model.PostText, model.TagsText,
                        Convert.ToString(Session["UserLogin"]));
                }
            }
            else
            {
                TempData["NullInputException"] = "1";
            }

            return RedirectToAction("PersonalBlog");
        }

        /// <summary>
        /// Show comments of post
        /// </summary>
        /// <param name="postId">Id of post</param>
        /// <returns>View of list comments of selected post</returns>
        [Authorize]
        public ActionResult PostComments(int? postId)
        {
            Datareload();
            
            var post = _postService.GetPosts().SingleOrDefault(w => w.Id == postId);
            if (post == null)
            {
                return View("Error");
            }
            
            IEnumerable<CommentDTO> commentDtos = _commentService.GetComments();
            var mapper2 = new MapperConfiguration(cfg => cfg.CreateMap<CommentDTO, Comment>()).CreateMapper();
            var comments = mapper2.Map<IEnumerable<CommentDTO>, List<Comment>>(commentDtos);

            var curUserId = User.Identity.GetUserId();
            var curUser = UserManager.Users.SingleOrDefault(w => w.Id == curUserId);
            var curUserVM = new UserViewModel
            {
                Id = curUserId,
                Role = curUser.Role
            };
            
            TempData.Clear();
            TempData["PostId"] = postId;
            TempData["Comments"] = comments.Where(w => w.PostId == postId);
            
            return View(curUserVM);
        }

        /// <summary>
        /// Finds the users
        /// </summary>
        /// <param name="loginPart"></param>
        /// <returns>View with users</returns>
        [Authorize]
        [ValidateInput(false)]
        public ActionResult FindUsers(string loginPart)
        {
            TempData["Users"] = UserManager.Users.ToList();
            return View(loginPart as object);
        }

        /// <summary>
        /// Leave's comment
        /// </summary>
        /// <param name="commentText">string with text of comment</param>
        /// <returns>If success redirects to main page</returns>
        [Authorize]
        public ActionResult LeaveComment(string commentText)
        {
            var post = _postService.GetPosts().SingleOrDefault(w => w.Id == (int) TempData["PostId"]);
            if (post == null)
            {
                return View("Error");
            }
            
            var curUserId = User.Identity.GetUserId();
            _commentService.MakeComment((int) TempData["PostId"], commentText, curUserId,
                UserManager.Users.FirstOrDefault(w => w.Id == curUserId)?.Login);
            if (!string.IsNullOrEmpty(Request.UrlReferrer?.ToString()))
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Delete entity of comment
        /// </summary>
        /// <param name="commentId">Comment id</param>
        /// <returns>If success redirects to url referrer</returns>
        [HttpGet]
        [Authorize]
        public ActionResult DeleteComment(string commentId)
        {
            _commentService.DeleteComment(Convert.ToInt32(commentId));
            
            return Redirect(Request.UrlReferrer?.ToString());
        }

        /// <summary>
        /// Edit entity of post
        /// </summary>
        /// <param name="postId">id of post</param>
        /// <returns>Editing post view</returns>
        /// <exception cref="Exception">Not found or permission exception</exception>
        [HttpGet]
        [Authorize]
        public ActionResult EditPost(string postId)
        {
            try
            {
                var curUserId = User.Identity.GetUserId();
                if (!_postService.GetPosts().Where(w => Convert.ToString(w.Id) == postId).Any())
                {
                    throw new Exception("404");
                }

                if (Convert.ToString(_postService.GetPosts().SingleOrDefault(w => Convert.ToString(w.Id) == postId)
                    .UserId) != curUserId && UserManager.Users.SingleOrDefault(w => w.Id == curUserId)?.Role != "Admin")
                {
                    return View("ErrorPermission");
                }

                Session.Clear();
                Session["EditPostId"] = postId;
                return View();
            }
            catch
            {
                return View("Error");
            }
        }

        /// <summary>
        /// Change post text 
        /// </summary>
        /// <param name="model">Model from view</param>
        /// <returns>View of personal blog</returns>
        [Authorize]
        [HttpPost]
        public ActionResult ChangePostData(PostViewModel model)
        {
            _postService.ChangePostText((string) Session["EditPostId"], model.PostText);

            return RedirectToAction("PersonalBlog");
        }
    }
}