﻿using System.Collections.Generic;

namespace PersonalBlog.Models
{
    public class PostViewModel
    {
        public string PostText { get; set; }
        public string TagsText { get; set; }
        public bool IsMe { get; set; }
    }
}