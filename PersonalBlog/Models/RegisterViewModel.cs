﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PersonalBlog.Models
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        [Compare("Password", ErrorMessage = "Password are different")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string Login { get; set; }

        [Required]
        public DateTime? Birthdate { get; set; }

        [Required]
        public string Phone { get; set; }
    }
}