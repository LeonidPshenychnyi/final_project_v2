﻿using Ninject.Modules;
using PersonalBlog.BLL.Interfaces;
using PersonalBlog.BLL.Services;

namespace PersonalBlog.Util
{
    public class PostModule : NinjectModule
    {
        /// <summary>
        /// Load data with Ninject
        /// </summary>
        public override void Load()
        {
            Bind<IPostService>().To<PostService>();
            Bind<ICommentService>().To<CommentService>();
        }
    }
}