﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PersonalBlog.DAL.Entities;

namespace PersonalBlog.DAL.Identity
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public ApplicationContext(string connectionString) : base(connectionString) { }
        
        public static ApplicationContext Create()
        {
            return new ApplicationContext("BlogDbConnection");
        }
    }
}