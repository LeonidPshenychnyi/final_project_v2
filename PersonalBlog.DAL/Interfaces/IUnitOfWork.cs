﻿using System;
using PersonalBlog.DAL.Entities;

namespace PersonalBlog.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Post> Posts { get; }
        IRepository<Comment> Comments { get; }
        void Save();
    }
}