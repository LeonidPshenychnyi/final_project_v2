﻿using System;

namespace PersonalBlog.DAL.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public Guid Userid { get; set; }
        public string UserLogin { get; set; }
        public int PostId { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
    }
}