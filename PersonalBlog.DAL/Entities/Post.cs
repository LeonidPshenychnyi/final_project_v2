﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PersonalBlog.DAL.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public DateTime? PostTime { get; set; }
        
        public string PostText { get; set; }
        public string TagsText { get; set; }
        public int CommentsCount { get; set; }
        public int LikesCount { get; set; }
        
        public List<Guid> LikedUsers { get; set; }
        public List<int> Comments { get; set; }
        
        public Guid UserId { get; set; }
        public string UserLogin { get; set; }
    }
}