﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.DAL.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        private ApplicationContext db;
 
        public CommentRepository(ApplicationContext context)
        {
            db = context;
        }
        public IEnumerable<Comment> GetAll()
        {
            return db.Comments;
        }

        public Comment Get(int id)
        {
            return db.Comments.Find(id);
        }

        public IEnumerable<Comment> Find(Func<Comment, bool> predicate)
        {
            return db.Comments.Where(predicate).ToList();
        }

        public void Create(Comment item)
        {
            db.Comments.Add(item);
        }

        public void Update(Comment item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment != null)
                db.Comments.Remove(comment);
        }
    }
}