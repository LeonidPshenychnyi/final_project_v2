﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.DAL.Repositories
{
    public class PostRepository : IRepository<Post>
    {
        private ApplicationContext db;
 
        public PostRepository(ApplicationContext context)
        {
            db = context;
        }
        public IEnumerable<Post> GetAll()
        {
            return db.Posts;
        }

        public Post Get(int id)
        {
            return db.Posts.Find(id);
        }

        public IEnumerable<Post> Find(Func<Post, bool> predicate)
        {
            return db.Posts.Where(predicate).ToList();
        }

        public void Create(Post item)
        {
            db.Posts.Add(item);
        }

        public void Update(Post item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Post book = db.Posts.Find(id);
            if (book != null)
                db.Posts.Remove(book);
        }
    }
}