﻿using System;
using PersonalBlog.DAL.Entities;
using PersonalBlog.DAL.Identity;
using PersonalBlog.DAL.Interfaces;

namespace PersonalBlog.DAL.Repositories
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;
        private PostRepository _postRepository;
        private CommentRepository _commentRepository;

        public EfUnitOfWork()
        {
            db = new ApplicationContext("BlogDbConnection");
        }

        public IRepository<Post> Posts
        {
            get
            {
                if (_postRepository == null)
                {
                    _postRepository = new PostRepository(db);
                }
                return _postRepository;
            }
        }

        public IRepository<Comment> Comments
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(db);
                }
                return _commentRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }
        
        private bool disposed = false;
 
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }
 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}