﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PersonalBlog.DAL.Identity
{
    public class BirthdateValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            var val = (DateTime)value;

            if (val > DateTime.Now)
                return false;

            return true;
        }
    }
}