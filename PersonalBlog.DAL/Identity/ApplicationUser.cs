﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using PersonalBlog.DAL.Entities;

namespace PersonalBlog.DAL.Identity
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        public string Role { get; set; }
        
        public string Login { get; set; }
        
        [Required]
        public DateTime? Birthdate { get; set; }
        
        public DateTime RegistrationDate { get; set; }
        
        public string City { get; set; }
        public string Languages { get; set; }
        public string About { get; set; }
        public ApplicationUser()
        {
            
        }
    }
}